import random, string, pyperclip

#demander combien de caractère sera le mot de passe
nb_caractere = input(" Combien de caractère doit contenir votre mot de passe ? ")

#vérification si la saisie est un chiffre, un nombre
if  (nb_caractere.isdigit()):
    nb_caractere=int(nb_caractere)# transformer la valeur saisie en integer
    #si la demande est inferieur ou égale à 3 rien
    if int(nb_caractere) <= 3:
        print ("désolé mais il y a un minimum de 4 caractères ")
    else:
        #génération du mot de passe
        mdp = ''.join(random.choices(string.ascii_letters + string.digits, k=nb_caractere))
        #afficher le mot de passe
        print(mdp)

        #copie le mot de passe dans le presse papier
        pyperclip.copy(mdp)
else:
    print ("Ce n'est pas une saisie numérique")
