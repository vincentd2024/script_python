#importation des moduls

import os
import glob
import youtube_dl
import shutil

#option 1 telechargement
def telecharge():
    url = input(" Url ?")#recuperation de l url
    ydl_opts = {}
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([url])#telechargement de l url

#option 2 delpacer des fichiers
def deplacer():
    #demander l'extension de fichier
    extension = input ("extension de fichier (par défaut: *.mp4) ?")
    if extension =="":
        extension="*.mp4" # si la réponse est vide on utilisera *.mp4
    else:
        pass
    #demander le dossier source
    source = input ("Le dossier source (par défaut:F:/Python/Divers/) ?")
    if source =="":
        source = "F:/Python/Divers/" # si pas de saisie, on défini le dossier par défaut
    else:
        pass
    #demande de la destination
    destination = input ("Destination (par défaut F:/video/)?")
    if destination =="":
        destination = "F:/video/" # si pas de saisie, on défini le dossier par défaut
    else:
        pass

    lasource = (source+extension)#concaténation source + extension
    print ("Source = "+lasource)

    lesfichiers = (glob.glob(lasource)) #mettre tous les fichiers dans une variable
    #calculer le nombre de fichiers:
    #j=globals()
    j=0
    for t in lesfichiers:
        j +=1# ajoute 1 pour chaque fichier trouve
    print('il y a '+str(j)+' fichie(s) à déplacer')#afficher le nombre de fichier trouvé
    if j > 0 :
        #vérification de l'existence du dossier source et destination
        if os.path.exists(source) and os.path.exists(destination):
            print ("les dossiers source et destination existent")
            reponse = input ('voulez vous vraiment déplacer les '+str(j)+' fichiers(s)')
            if reponse =="o" or reponse=="O":
                #boucle recupérant chaque le chement de chaque fichier dans la variable lesfichiers
                for file in lesfichiers:
                    print("source : " + file)
                    print("destination : " + destination)
                    shutil.move (file, destination)#déplace le fichiers
            else:
                pass
        else:
            print ("il y a un problème avec la source ou la destination")
    else:
        print("il n'y a rien a déplacer")
run = True
while run == True:
      print("     --- Youtube Download ----        ")
      print(" 1 - Télécharger un fichier sur youtube")
      print(" 2 - Déplacer tous les fichiers avec l'extension    ")
      print(" 3 - sortir du programme ")
      choix = input( "?")
      if choix =="1":
        telecharge()
      elif choix =="2":
        deplacer()
      else:
        run = False
