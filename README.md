# script_python

scripts python

- Le 08/07/2021 ajout du script download_Youtube.py
  Ce script permet de télécharger des fichiers provenant de Youtube.
  Il permet également de déplacer tous les fichiers ayant la même extension et se trouvant dans le même répertoire.
- Le 15/07/2021 ajout d'un script pour générer des mots de passes
- Le 16/07/2021 Ajout du module pyperclip pour copier le mot de passe généré dans le presse papier.
